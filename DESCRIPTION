Package: destiny
Type: Package
Title: Creates diffusion maps
Version: 3.20.0
Date: 2014-12-19
Authors@R: c(
	person('Philipp', 'Angerer', NULL, 'phil.angerer@gmail.com', c('cre', 'aut'), comment = c(ORCID = '0000-0002-0369-2888')),
	person('Laleh', 'Haghverdi', NULL, 'laleh.haghverdi@helmholtz-muenchen.de', 'ctb'),
	person('Maren', 'Büttner', NULL, 'maren.buettner@helmholtz-muenchen.de', 'ctb', comment = c(ORCID = '0000-0002-6189-3792')),
	person('Fabian', 'Theis', NULL, 'fabian.theis@helmholtz-muenchen.de', 'ctb', comment = c(ORCID = '0000-0002-2419-1943')),
	person('Carsten', 'Marr', NULL, 'carsten.marr@helmholtz-muenchen.de', 'ctb', comment = c(ORCID = '0000-0003-2154-4552')),
	person('Florian', 'Büttner', NULL, 'f.buettner@helmholtz-muenchen.de', 'ctb', comment = c(ORCID = '0000-0001-5587-6761')))
Description: Create and plot diffusion maps.
License: GPL-3
URL: https://theislab.github.io/destiny/,
        https://github.com/theislab/destiny/,
        https://www.helmholtz-muenchen.de/icb/destiny,
        https://bioconductor.org/packages/destiny,
        https://doi.org/10.1093/bioinformatics/btv715
BugReports: https://github.com/theislab/destiny/issues
Encoding: UTF-8
Depends: R (>= 3.4.0)
Imports: methods, graphics, grDevices, grid, utils, stats, Matrix, Rcpp
        (>= 0.10.3), RcppEigen, RSpectra (>= 0.14-0), irlba,
        pcaMethods, Biobase, BiocGenerics, SummarizedExperiment,
        SingleCellExperiment, ggplot2, ggplot.multistats, rlang, tidyr,
        tidyselect, ggthemes, VIM, knn.covertree, proxy, RcppHNSW,
        smoother, scales, scatterplot3d
LinkingTo: Rcpp, RcppEigen, grDevices
SystemRequirements: C++11
NeedsCompilation: yes
Enhances: rgl, SingleCellExperiment
Suggests: knitr, rmarkdown, igraph, testthat, FNN, tidyverse,
        gridExtra, cowplot, conflicted, viridis, rgl, scRNAseq,
        org.Mm.eg.db, scran, repr
VignetteBuilder: knitr
biocViews: CellBiology, CellBasedAssays, Clustering, Software,
        Visualization
Collate: 'RcppExports.R' 'aaa.r' 'accessor-generics.r' 'censoring.r'
        'colorlegend.r' 'cube_helix.r' 'dataset-helpers.r'
        'destiny-package.r' 's4-unions.r' 'dist-matrix-coerce.r'
        'sigmas.r' 'diffusionmap.r' 'diffusionmap-methods-accession.r'
        'diffusionmap-methods.r' 'plothelpers.r'
        'diffusionmap-plotting.r' 'dpt-branching.r' 'dpt-helpers.r'
        'dpt.r' 'dpt-methods-matrix.r' 'dpt-methods.r' 'utils.r'
        'dpt-plotting.r' 'eig_decomp.r' 'expressionset-helpers.r'
        'find_dm_k.r' 'gene-relevance.r' 'gene-relevance-methods.r'
        'gene-relevance-plotting-differential-map.r'
        'gene-relevance-plotting-gr-map.r'
        'gene-relevance-plotting-rank.r' 'gene-relevance-plotting.r'
        'guo-data.r' 'knn.r' 'l_which.r' 'methods-coercion.r'
        'methods-extraction.r' 'methods-update.r' 'predict.r'
        'projection-dist.r' 'rankcor.r' 'sigmas-plotting.r'
RoxygenNote: 7.3.2
git_url: https://git.bioconductor.org/packages/destiny
git_branch: RELEASE_3_20
git_last_commit: b0e8a1c
git_last_commit_date: 2024-11-15
Repository: Bioconductor 3.20
Date/Publication: 2024-11-15
Packaged: 2024-11-15 22:37:21 UTC; biocbuild
Author: Philipp Angerer [cre, aut] (<https://orcid.org/0000-0002-0369-2888>),
  Laleh Haghverdi [ctb],
  Maren Büttner [ctb] (<https://orcid.org/0000-0002-6189-3792>),
  Fabian Theis [ctb] (<https://orcid.org/0000-0002-2419-1943>),
  Carsten Marr [ctb] (<https://orcid.org/0000-0003-2154-4552>),
  Florian Büttner [ctb] (<https://orcid.org/0000-0001-5587-6761>)
Maintainer: Philipp Angerer <phil.angerer@gmail.com>
