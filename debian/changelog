r-bioc-destiny (3.20.0-3) unstable; urgency=medium

  * Team upload.
  * Skip unsatifiable test dependencies for the autodep8 tests.

 -- Michael R. Crusoe <crusoe@debian.org>  Tue, 21 Jan 2025 16:11:23 +0100

r-bioc-destiny (3.20.0-2) unstable; urgency=medium

  * Team upload.
  * Upload to unstable.

 -- Michael R. Crusoe <crusoe@debian.org>  Mon, 13 Jan 2025 14:37:17 +0100

r-bioc-destiny (3.20.0-1) experimental; urgency=medium

  * Team upload.
  * New upstream version
  * Standards-Version: 4.7.0 (routine-update)
  * dh-update-R to update Build-Depends (routine-update)

 -- Michael R. Crusoe <crusoe@debian.org>  Fri, 29 Nov 2024 11:10:36 +0100

r-bioc-destiny (3.18.0-1) unstable; urgency=medium

  * Team upload.
  * Upload to unstable.

 -- Michael R. Crusoe <crusoe@debian.org>  Sun, 18 Aug 2024 11:32:47 +0200

r-bioc-destiny (3.18.0-1~0exp) experimental; urgency=medium

  * Team upload
  * New upstream version
  * dh-update-R to update Build-Depends (routine-update)
  * Set upstream metadata fields: Archive.
  * d/control: Skip building on 32-bit systems.

 -- Michael R. Crusoe <crusoe@debian.org>  Sat, 13 Jul 2024 16:13:44 +0200

r-bioc-destiny (3.16.0-1) unstable; urgency=medium

  * New upstream version

 -- Andreas Tille <tille@debian.org>  Thu, 30 Nov 2023 15:55:21 +0100

r-bioc-destiny (3.14.0-1) unstable; urgency=medium

  * New upstream version
  * Standards-Version: 4.6.2 (routine-update)
  * dh-update-R to update Build-Depends (routine-update)

 -- Andreas Tille <tille@debian.org>  Wed, 26 Jul 2023 22:10:41 +0200

r-bioc-destiny (3.12.0-1) unstable; urgency=medium

  * New upstream version
  * Reduce piuparts noise in transitions (routine-update)
  * lintian-overrides (See lintian bug #1017966)

 -- Andreas Tille <tille@debian.org>  Mon, 21 Nov 2022 16:21:52 +0100

r-bioc-destiny (3.10.0-1) unstable; urgency=medium

  * Team upload.
  * New upstream version 3.10.0
  * Bump Standards-Version to 4.6.1 (no changes needed)

 -- Nilesh Patra <nilesh@debian.org>  Mon, 16 May 2022 10:24:25 +0530

r-bioc-destiny (3.8.1-1) unstable; urgency=medium

  * New upstream version

 -- Andreas Tille <tille@debian.org>  Sat, 12 Feb 2022 07:48:39 +0100

r-bioc-destiny (3.8.0-2) unstable; urgency=medium

  * Provide autopkgtest-pkg-r.conf to make sure testthat will be found
  * Drop debian/tests/control and rely on autopkgtest-pkg-r
  * Disable reprotest

 -- Andreas Tille <tille@debian.org>  Mon, 20 Dec 2021 11:11:02 +0100

r-bioc-destiny (3.8.0-1) unstable; urgency=medium

  * New upstream version
  * Standards-Version: 4.6.0 (routine-update)
  * Trim trailing whitespace.
  * Set upstream metadata fields: Bug-Database, Bug-Submit, Repository,
    Repository-Browse.

 -- Andreas Tille <tille@debian.org>  Mon, 20 Dec 2021 08:54:29 +0100

r-bioc-destiny (3.4.0-1) unstable; urgency=medium

  * New upstream version

 -- Andreas Tille <tille@debian.org>  Wed, 04 Nov 2020 08:40:19 +0100

r-bioc-destiny (3.2.0-2) unstable; urgency=medium

  * debhelper-compat 13 (routine-update)

 -- Andreas Tille <tille@debian.org>  Tue, 27 Oct 2020 22:12:36 +0100

r-bioc-destiny (3.2.0-1) unstable; urgency=medium

  * Initial release (closes: #969997)

 -- Andreas Tille <tille@debian.org>  Wed, 09 Sep 2020 21:24:08 +0200
